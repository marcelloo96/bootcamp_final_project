Sz�ks�g lesz egy localhoston fut� MYSQL adatb�zisra:

-------MYSQL-------

create database if not exists student_db;

use student_db;

create user 'smartprogrammer'@'localhost' identified by 'verySmartPassword';

grant all on student_db.* to 'smartprogrammer'@'localhost';

-------MYSQL-------

A DatabaseSeeder oszt�lyban kikommentezve vannak gener�lt felhaszn�l�k. 
"spring.jpa.hibernate.ddl-auto=update" lett be�ll�tva, 
�gy az els� futtat�s ut�n a duplik�ci� elker�l�se miatt c�lszer� �jra kikommentezni ezeket, 
vagy az 'update' kulcssz�t 'create'-re cser�lni.

Funkci�k:
-Minden Felhaszn�l� kilist�z�sa
-Szak szerinti sz�r�s (Contenteditable button, �rt�k�t �t�rva m�s szakra is lehet sz�rni)
-Felhaszn�l�k t�rl�se
-Felhaszn�l� besz�r�sa (Postmannel a localhost:8080/students/create/ -en)
	POST-JSON:
	{	"name": "Eric Cartman",
		"department": "Cooking",
		"numOfSemestersSpent": "2"
	
}
		
Felhaszn�lt technol�gi�k:
-Thymeleaf
-Hibernate
-MySql
-Angular
-Bootstrap