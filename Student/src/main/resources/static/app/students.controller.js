(function () {
    'use strict';

    angular
        .module('app')
        .controller('StudentsController', StudentsController);

    StudentsController.$inject = ['$http'];

    function StudentsController($http) {
        var vm = this;

        vm.students = [];
        vm.getAll = getAll;
        vm.getProgrammer = getProgrammer;
        vm.deleteStudent = deleteStudent;

        init();

        function init(){
            getAll();

        }

        function getAll(){
            var url = "/students/all";
            var studentsPromise = $http.get(url);
            studentsPromise.then(function(response){
                vm.students = response.data;
            });
        }

        function getProgrammer(){
        	var filtered = document.getElementById("filter").innerHTML;
            var url = "/students/department/" + filtered;
            var studentsPromise = $http.get(url);
            studentsPromise.then(function(response){
                vm.students = response.data;
            });
        }

        function deleteStudent(id){
            var url = "/students/delete/" + id;
            $http.post(url).then(function(response){
                vm.students = response.data;
            });
        }
    }
})();
