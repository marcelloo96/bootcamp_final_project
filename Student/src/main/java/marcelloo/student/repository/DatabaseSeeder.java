package marcelloo.student.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import marcelloo.student.model.StudentBean;

@Component
public class DatabaseSeeder implements CommandLineRunner {
	private StudentRepository studentRepository;
	
	@Autowired
	public DatabaseSeeder(StudentRepository studentRepository) {
		this.studentRepository=studentRepository;
	}
	
	@Override
	public void run(String... args) throws Exception {
		/*List<StudentBean> students=new ArrayList<>();
		
		students.add(new StudentBean("Kiss Elek","Programmer",3));
		students.add(new StudentBean("Maczák Bálint","Law",2));
		students.add(new StudentBean("Szabó Máté","English",3));
		students.add(new StudentBean("Kovács Marcell","Programmer",4));
		students.add(new StudentBean("Kasie Ogles","English",3));
		students.add(new StudentBean("Karlene Boland","English",2));
		students.add(new StudentBean("Katherine Diaz","Medicine",3));
		students.add(new StudentBean("Smart Programmer","easteregg",4));
		students.add(new StudentBean("Ta Roiger","History",3));
		students.add(new StudentBean("Emmanuel Gum","Medicine",2));
		students.add(new StudentBean("Clarissa Hasegawa","Mathematics",3));
		students.add(new StudentBean("Burt Roberson","Law",4));
		students.add(new StudentBean("Lorri Campana","Programmer",3));
		students.add(new StudentBean("Ngan Schecter","History",2));
		students.add(new StudentBean("Justin Depriest","English",3));
		students.add(new StudentBean("Tanna Streetman","Biology",4));
		students.add(new StudentBean("Kellie Lingle","Programmer",3));
		students.add(new StudentBean("Jillian Weibel","Mathematics",2));
		students.add(new StudentBean("Melody Poeppelman","Biology",3));
		students.add(new StudentBean("Piedad Leathers","Law",4));
		students.add(new StudentBean("Ouida Poppell","Programmer",3));
		students.add(new StudentBean("Marita Mcgaha","Mathematics",2));
		students.add(new StudentBean("Mitsue Clickner","History",3));
		students.add(new StudentBean("Kim Shulman","Biology",4));
		students.add(new StudentBean("Tracy Derouin","Programmer",3));
		students.add(new StudentBean("Joesph Martyn","Mathematics",2));
		students.add(new StudentBean("Trudie Lebouef","Medicine",3));
		students.add(new StudentBean("Ed Henrikson","Law",4));
		students.add(new StudentBean("Beatrice Schier","History",3));
		students.add(new StudentBean("Edythe Miah","Biology",2));
		
		studentRepository.saveAll(students);*/
	}

}
