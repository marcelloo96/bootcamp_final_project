package marcelloo.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import marcelloo.student.model.StudentBean;
import marcelloo.student.repository.StudentRepository;

@RestController
@RequestMapping(value="/students")
public class StudentController {

	private StudentRepository studentRepository;
	
	@Autowired
	public StudentController(StudentRepository studentRepository) {
		this.studentRepository=studentRepository;
		
		
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public List<StudentBean> getAll(){
		return studentRepository.findAll();
	}
	
	@RequestMapping(value="/department/{department}", method=RequestMethod.GET)
	public List<StudentBean> getSpecific(@PathVariable String department){
		return studentRepository.findBydepartmentLike(department);
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public List<StudentBean> create(@RequestBody StudentBean student){

		studentRepository.save(student);
		return studentRepository.findAll();
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.POST)
	public List<StudentBean> deleteStudent(@PathVariable long id){

		studentRepository.deleteById(id);
		return studentRepository.findAll();
	}	
	
}
