package marcelloo.student.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StudentBean {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String name;
	private String department;
	private int NumOfSemestersSpent;
	
	public StudentBean() {}
	
	public StudentBean(String name, String department, int numOfSemestersSpent) {
		this.name = name;
		this.department = department;
		NumOfSemestersSpent = numOfSemestersSpent;
	}

	public String getName() {
		return name;
	}

	public String getDepartment() {
		return department;
	}

	public int getNumOfSemestersSpent() {
		return NumOfSemestersSpent;
	}

	public long getId() {
		return id;
	}

	
	
}
