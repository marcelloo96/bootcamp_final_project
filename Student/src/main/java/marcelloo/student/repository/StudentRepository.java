package marcelloo.student.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import marcelloo.student.model.StudentBean;

@Repository
public interface StudentRepository extends JpaRepository<StudentBean, Long>{
	List<StudentBean> findBydepartmentLike(String department);
	

}
